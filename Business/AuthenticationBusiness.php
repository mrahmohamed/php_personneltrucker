<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:56 PM
 */

namespace Business;

require_once dirname(dirname(__FILE__)) . '/DAO/AuthenticationDAO.php';

use DAO\AuthenticationDAO;
use Model\Authentication;

use ArrayObject,ArrayIterator;

class AuthenticationBusiness
{
    private $authenticationDAO;

    function __construct()
    {
        $this->authenticationDAO = new AuthenticationDAO();
    }
    /* ----------------------- * * ----------------------- */
    public function display()
    {
        $list = $this->$authenticationDAO->select();
        $iterator = $list->getIterator();

        $str_html  = "<table border='1' style='text-align: center;'>
                          <tr>
                              <td>Username</td>
                              <td>Edit</td>
                          </tr>
                            ";

        while ($iterator->valid())
        {
            $authentication = $iterator->current();

            $str_html .=  "<tr>
                            <td>{$authentication->getUsername()}</td>
                            <td><a href='#'><img src='../View/Public/images/b_edit.png'></a></td>
                          </tr>";

            $iterator->next();
        }

        print $str_html."</table>";
    }

}