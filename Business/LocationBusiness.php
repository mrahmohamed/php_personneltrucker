<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:56 PM
 */

namespace Business;

require_once dirname(dirname(__FILE__)) . '/DAO/LocationDAO.php';

use DAO\LocationDAO;
use Model\Location;

use ArrayObject,ArrayIterator;

class LocationBusiness
{
    private $locationDAO;

    function __construct()
    {
        $this->locationDAO = new LocationDAO();
    }
    /* ----------------------- * * ----------------------- */
    public function display()
    {
        $list = $this->$locationDAO->select();
        $iterator = $list->getIterator();

        $str_html  = "<table border='1' style='text-align: center;'>
                          <tr>
                              <td>Location</td>
                              <td>Supprim&eacute;</td>
                              <td>Edit</td>
                          </tr>
                            ";

        while ($iterator->valid())
        {
            $location = $iterator->current();

            $str_html .=  "<tr>
                            <td>{$location->toString()}</td>
                            <td><a href='#'><img src='../View/Public/images/b_edit.png'></a></td>
                            <td><a href='#'><img src='../View/Public/images/b_drop.gif'></a></td>
                          </tr>";

            $iterator->next();
        }

        print $str_html."</table>";
    }

}