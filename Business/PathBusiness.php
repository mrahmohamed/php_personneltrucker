<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:56 PM
 */

namespace Business;

require_once dirname(dirname(__FILE__)) . '/DAO/PathDAO.php';

use DAO\PathDAO;
use Model\Path;

use ArrayObject,ArrayIterator;

class PathBusiness
{
    private $pathDAO;

    function __construct()
    {
        $this->pathDAO = new PathDAO();
    }
    /* ----------------------- * * ----------------------- */
    public function display()
    {
        $list = $this->pathDAO->select();
        $iterator = $list->getIterator();

        $str_html  = "<table border='1' style='text-align: center;'>
                          <tr>
                              <td>DateTime</td>
                              <td>Location</td>
                              <td>Personnel</td>
                              <td>Supprim&eacute;</td>
                              <td>Edit</td>
                          </tr>
                            ";

        while ($iterator->valid())
        {
            $path = $iterator->current();

            $str_html .=  "<tr>
                            <td>{$path->getCurrentDateTime()}</td>
                            <td>{$path->getLocation()->toString()}</td>
                            <td>{$path->getPersonnel()->toString()}</td>
                            <td><a href='#'><img src='../View/Public/images/b_edit.png'></a></td>
                            <td><a href='#'><img src='../View/Public/images/b_drop.gif'></a></td>
                          </tr>";

            $iterator->next();
        }

        print $str_html."</table>";
    }

}