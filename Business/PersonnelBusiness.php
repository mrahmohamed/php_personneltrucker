<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:56 PM
 */

namespace Business;

require_once dirname(dirname(__FILE__)) . '/DAO/PersonnelDAO.php';

use DAO\PersonnelDAO;
use Model\Personnel;

use ArrayObject,ArrayIterator;

class PersonnelBusiness
{

    private $personnelDAO;

    function __construct()
    {
        $this->personnelDAO = new PersonnelDAO();
    }
    /* ----------------------- * * ----------------------- */
    public function display()
    {
        $list = $this->personnelDAO->select();
        $iterator = $list->getIterator();

        $str_html  = "<table border='1' style='text-align: center;'>
                          <tr>
                              <td>Nom</td>
                              <td>CNE</td>
                              <td>Adresse</td>
                              <td>Tel</td>
                              <td>E-mail</td>
                              <td>Supprim&eacute;</td>
                              <td>Edit</td>
                          </tr>
                            ";

        while ($iterator->valid())
        {
            $personnel = $iterator->current();

            $str_html .=  "<tr>
                            <td>{$personnel->getFullName()}</td>
                            <td>{$personnel->getCNE()}</td>
                            <td>{$personnel->getAddress()}</td>
                            <td>{$personnel->getPhone()}</td>
                            <td>{$personnel->getEmail()}</td>
                            <td><a href='#'><img src='../View/Public/images/b_edit.png'></a></td>
                            <td><a href='#'><img src='../View/Public/images/b_drop.gif'></a></td>
                          </tr>";

            $iterator->next();
        }

        print $str_html."</table>";
    }

}