<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:56 PM
 */


namespace Business;

require_once dirname(dirname(__FILE__)) . '/DAO/SchoolDAO.php';

use DAO\SchoolDAO;
use Model\School;

use ArrayObject,ArrayIterator;


class SchoolBusiness
{

    private $schoolDAO;

    function __construct()
    {
        $this->schoolDAO = new SchoolDAO();
    }
    /* ----------------------- * * ----------------------- */
    public function display()
    {
        $list = $this->schoolDAO->select();
        $iterator = $list->getIterator();

        $str_html  = "<table border='1' style='text-align: center;'>
                          <tr>
                              <td>Coll&egrave;;ge</td>
                              <td>Adresse</td>
                              <td>Coordonn&eacute;es</td>
                              <td>Supprim&eacute;</td>
                              <td>Edit</td>
                          </tr>
                            ";

        while ($iterator->valid())
        {
            $school = $iterator->current();

            $str_html .=  "<tr>
                            <td>{$school->getSchoolName()}</td>
                            <td>{$school->getAddress()}</td>
                            <td>{$school->getLocation()->toString()}</td>
                            <td><a href='#'><img src='../View/Public/images/b_edit.png'></a></td>
                            <td><a href='#'><img src='../View/Public/images/b_drop.gif'></a></td>
                          </tr>";

            $iterator->next();
        }

        print $str_html."</table>";
    }
}