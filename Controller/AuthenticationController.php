<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:55 PM
 */

namespace Controller;

$root = dirname(dirname(__FILE__));

require_once $root . "/DAO/AuthenticationDAO.php";
require_once $root . "/Model/Authentication.php";

use DAO\AuthenticationDAO;
use Model\Authentication;


class AuthenticationController
{

    private $array; // $_POST or $_GET
    private $type;

    private $authentication;
    private $authenticationDAO;

    /* ----------------------- * * ----------------------- */

    /**
     * @param $array
     */
    function __construct($array)
    {
        $this->array = $array;
        $this->type = $this->array["type"];

        $this->authenticationDAO  = new AuthenticationDAO();


        if(isset($this->array["id"]))        $this->authentication->setId($this->array['id']);
        if(isset($this->array["username"]))  $this->authentication->setUsername($this->array['username']);
        if(isset($this->array["password"]))  $this->authentication->setPassword($this->array['password']);

        switch($this->type)
        {
            case 0: return $this->save();    break;
            case 1: return $this->update();  break;
            case -1: return $this->remove();  break;

            default: print 'error' ;
        }
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @return int|null|string
     */
    private function save()
    {
        return $this->authenticationDAO->insert($this->authentication);
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @return mixed
     */
    private function update()
    {
        return $this->authenticationDAO->update($this->authentication);
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @return mixed
     */
    private function remove()
    {
        return $this->authenticationDAO->delete($this->authentication);
    }
}