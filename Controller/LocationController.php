<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:54 PM
 */

namespace Controller;

$root = dirname(dirname(__FILE__));
require_once $root . "/DAO/LocationDAO.php";
require_once $root . "/Model/Location.php";

use Model\Location;
use DAO\LocationDAO;

class LocationController
{

    private $array; // $_POST or $_GET
    private $type;

    private $location;
    private $locationDAO;

    /**
     * @param $array
     */
    function __construct($array)
    {
        $this->array = $array;
        $this->type = $this->array["type"];

        $this->locationDAO = new LocationDAO();

        if(isset($this->array["id"]))         $this->location->setId($this->array['id']);
        if(isset($this->array["longitude"]))  $this->location->setUsername($this->array['longitude']);
        if(isset($this->array["password"]))   $this->location->setPassword($this->array['password']);


        switch($this->type)
        {
            case 0:  return $this->save();    break;
            case -1: return $this->remove();  break;

            default: print 'error' ;
        }
    }


    /**
     * @return int|null|string
     */
    private function save()
    {
        return $this->locationDAO->insert($this->location);
    }

    /**
     * @return bool|\mysqli_result
     */
    private function remove()
    {
        return $this->locationDAO->delete($this->location);
    }

}