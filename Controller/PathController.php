<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:54 PM
 */

namespace Controller;

$root = dirname(dirname(__FILE__));
require_once $root . "/DAO/LocationDAO.php";
require_once $root . "/Model/Location.php";

require_once $root . "/DAO/PersonnelDAO.php";
require_once $root . "/Model/Personnel.php";

require_once $root . "/DAO/PathDAO.php";
require_once $root . "/Model/Path.php";


use Model\Location;
use DAO\LocationDAO;

use Model\Personnel;
use DAO\PersonnelDAO;

use Model\Path;
use DAO\PathDAO;

class PathController
{

    private $array; // $_POST or $_GET
    private $type;

    private $location;
    private $locationDAO;

    private $path;
    private $pathDAO;

    private $personnel;
    private $personnelDAO;

    /* ----------------------- * * ----------------------- */

    /**
     * @param $array
     */
    function __construct($array)
    {
        $this->array = $array;
        $this->type = $this->array["type"];

        $this->pathDAO = new PathDAO();
        $this->locationDAO = new LocationDAO();
        $this->personnelDAO = new PersonnelDAO();

        if(isset($this->array["id"]))               $this->path->setId($this->array['id']);
        if(isset($this->array["currentDateTime"]))  $this->path->setCurrentDateTime($this->array['currentDateTime']);
        if(isset($this->array["location"]))         $this->path->setLocation($this->locationDAO->selectByID($this->array['location']));
        if(isset($this->array["personnelID"]))      $this->path->setPersonnel($this->personnelDAO->selectByID($this->array['personnel']));

        switch($this->type)
        {
            case 0:  return $this->save();    break;
            case -1: return $this->remove();  break;

            default: print 'error' ;
        }
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @return int|null|string
     */
    private function save()
    {
        return $this->pathDAO->insert($this->path);
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @return bool|\mysqli_result
     */
    private function remove()
    {
        return $this->pathDAO->delete($this->path);
    }

}