<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:54 PM
 */

namespace Controller;

$root = dirname(dirname(__FILE__));
require_once $root . "/DAO/PersonnelDAO.php";
require_once $root . "/Model/Personnel.php";

use Model\Personnel;
use DAO\PersonnelDAO;

class PersonnelController
{

    private $array; // $_POST or $_GET
    private $type;

    private $personnel;
    private $personnelDAO;

    /* ----------------------- * * ----------------------- */

    /**
     * @param $array
     */
    function __construct($array)
    {
        $this->array = $array;
        $this->type = $this->array["type"];

        $this->personnelDAO = new PersonnelDAO();

        if(isset($this->array["id"]))           $this->personnel->setId($this->array['id']);
        if(isset($this->array["fullName"]))     $this->personnel->setFullName($this->array['fullName']);
        if(isset($this->array["CNE"]))          $this->personnel->setCNE($this->array['CNE']);
        if(isset($this->array["address"]))      $this->personnel->setAddress($this->array['address']);
        if(isset($this->array["phone"]))        $this->personnel->setPhone($this->array['phone']);
        if(isset($this->array["email"]))        $this->personnel->setEmail($this->array['email']);

        switch($this->type)
        {
            case 0:  return $this->save();    break;
            case 1:  return $this->update();   break;
            case -1: return $this->remove();  break;

            default: print 'error' ;
        }
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @return int|null|string
     */
    private function save()
    {
        return $this->personnelDAO->insert($this->personnel);
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @return int|null|string
     */
    private function update()
    {
        return $this->personnelDAO->update($this->personnel);
    }

    /* ----------------------- * * ----------------------- */
    /**
     * @return bool|\mysqli_result
     */
    private function remove()
    {
        return $this->personnelDAO->delete($this->personnel);
    }

}