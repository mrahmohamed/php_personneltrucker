<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:54 PM
 */

namespace Controller;

$root = dirname(dirname(__FILE__));
require_once $root . "/DAO/LocationDAO.php";
require_once $root . "/Model/Location.php";

require_once $root . "/DAO/SchoolDAO.php";
require_once $root . "/Model/School.php";


use Model\Location;
use DAO\LocationDAO;

use Model\School;
use DAO\SchoolDAO;

class SchoolController
{

    private $array; // $_POST or $_GET
    private $type;

    private $location;
    private $locationDAO;

    private $school;
    private $schoolDAO;

    /* ----------------------- * * ----------------------- */

    /**
     * @param $array
     */
    function __construct($array)
    {
        $this->array = $array;
        $this->type = $this->array["type"];

        $this->locationDAO = new LocationDAO();
        $this->schoolDAO   = new SchoolDAO();

        if(isset($this->array["id"]))         $this->school->setId($this->array['id']);
        if(isset($this->array["schoolName"])) $this->school->setSchoolName($this->array['schoolName']);
        if(isset($this->array["address"]))    $this->school->setAddress($this->array['address']);
        if(isset($this->array["location"]))   $this->school->setLocation($this->locationDAO->selectByID($this->array['location']));

        switch($this->type)
        {

            case  0: return $this->save();  break;
            case  1: return $this->update();  break;
            case -1: return $this->remove();  break;

            default: print 'error' ;
        }
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @return int|null|string
     */
    private function save()
    {
        return $this->schoolDAO->insert($this->school);
    }
    /* ----------------------- * * ----------------------- */

    /**
     * @return int|null|string
     */
    private function update()
    {
        return $this->schoolDAO->update($this->school);
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @return bool|\mysqli_result
     */
    private function remove()
    {
        return $this->schoolDAO->delete($this->school);
    }

}