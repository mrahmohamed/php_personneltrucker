<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 6:22 PM
 */

namespace DAO;

$root = dirname(dirname(__FILE__));

require_once $root . '/DAO/DB/Connect.php';
require_once $root .'/Model/Authentication.php';

use DAO\Connect, Model\Authentication;

use ArrayObject;

class AuthenticationDAO
{
    public function __construct() {}

    /* ----------------------- * * ----------------------- */

    /**
     * @return ArrayObject
     */
    public function select()
    {
        $list = new ArrayObject(array ());

        $query = "Select * From `authentications`";

        if ($result = Connect::executeQuery($query))
        {
            while (list($id, $username, $password) = Connect::fetch($result))
            {
                $list->append(new Authentication($username, $password, $id));
            }
        }
        return $list;
    }

    /* ----------------------- * * ----------------------- */
    /**
     * @param $id
     * @return Authentication|null
     */
    public function selectByID($id)
    {
        $query ="Select * From `authentications` where `id` = '{$id}'; ";

        if ($result = Connect::executeQuery($query))
        {
            return (list($id, $username, $password) = Connect::fetch($result)) ?
                new Authentication($username, $password, $id) : null;
        }
        else
        {
            return null;
        }
    }

    /* ----------------------- * * ----------------------- */
    /**
     * @param $username
     * @return bool
     */
    private function exist($username)
    {
        $username = Connect::real_escape($username);

        $query = "Select * From `authentications` where `username` = '{$username}'; ";
        return ($result = Connect::executeQuery($query)) ? (Connect::fetch($result) != null) : false ;
    }

    /* ----------------------- * * ----------------------- */
    /**
     * @param Authentication $authentication
     * @return int|null|string
     */
    public function insert(Authentication $authentication)
    {
        $authentication->setUsername(Connect::real_escape($authentication->getUsername()));
        $authentication->setPassword(MD5($authentication->getPassword()));

        $query = "Insert into `authentications` values ( NULL, '{$authentication->getUsername()}','{$authentication->getPassword()}'); ";

        return (Connect::executeQuery($query)) ? Connect::insertedID() : null ;
    }

    /* ----------------------- * * ----------------------- */
    /**
     * @param Authentication $authentication
     * @return bool|\mysqli_result
     */
    public function updatePassword(Authentication $authentication)
    {
        $authentication->setPassword(MD5($authentication->getPassword()));

        $query = "update `authentications` set  `password` =  '{$authentication->getPassword()}' where `id` = '{$authentication->getId()}'; ";

        return Connect::executeQuery($query);
    }


    /* ----------------------- * * ----------------------- */

    public function update(Authentication $authentication)
    {
        $authentication->setPassword(MD5($authentication->getPassword()));

        $query = "update `authentications` set  `username` =  '{$authentication->getUsername()}', `password` =  '{$authentication->getPassword()}' where `id` = '{$authentication->getId()}'; ";

        return Connect::executeQuery($query);
    }


    /* ----------------------- * * ----------------------- */
    /**
     * @param Authentication $authentication
     * @return bool|\mysqli_result
     */
    public function delete(Authentication $authentication)
    {
        $query = "delete * from `authentications`  where `id` = '{$authentication->getId()}'; ";

        return Connect::executeQuery($query);
    }

    /* ----------------------- * * ----------------------- */
}
