<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 6:18 PM
 */

namespace DAO;

class Connect
{
    private $username;
    private $password;
    private $server;
    private $db_name;
    private static $cnxID;

    /* ---------------------------- ** ---------------------------- */
    private function __construct()
    {
        require_once 'db.config.inc.php';

        self::$cnxID    = mysqli_connect($this->server,$this->username, $this->password,$this->db_name);
    }
    /* ---------------------------- ** ---------------------------- */

    private static function getInstance()
    {
        if (self::$cnxID == null)   new Connect();

        return self::$cnxID ;
    }

    /* ---------------------------- ** ---------------------------- */
    /**
     * @param $query
     * @return bool|\mysqli_result
     */
    public static function executeQuery($query)
    {
        return mysqli_query(self::getInstance(),$query);
    }
    /* ---------------------------- ** ---------------------------- */
    public static function insertedID()
    {
        return mysqli_insert_id(self::$cnxID);
    }

    /* ---------------------------- ** ---------------------------- */
    public static function fetch($result)
    {
        return  mysqli_fetch_row($result);
    }
    /* ---------------------------- ** ---------------------------- */
    public static function fetchAll($result)
    {
        return  mysqli_fetch_all($result);
    }
    /* ---------------------------- ** ---------------------------- */
    public static function real_escape($escapeStr)
    {
        return  mysqli_real_escape_string(self::$cnxID,$escapeStr);
    }

    /* ---------------------------- ** ---------------------------- */


}
