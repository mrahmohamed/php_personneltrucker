<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 6:43 PM
 */

namespace DAO;

$root = dirname(dirname(__FILE__));

require_once $root . '/DAO/DB/Connect.php';
require_once $root .'/Model/Location.php';

use DAO\Connect;
use Model\Location;

use ArrayObject;

class LocationDAO
{
    public function __construct() {}

    /* ----------------------- * * ----------------------- */

    /**
     * @return ArrayObject
     */
    public function select()
    {
        $list = new ArrayObject(array ());

        $query = "Select * From `locations`";

        if ($result = Connect::executeQuery($query))
        {
            while (list($id, $longitude, $latitude) = Connect::fetch($result))
            {
                $list->append(new Location($longitude, $latitude, $id));
            }
        }
        return $list;
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param $id
     * @return Location|null
     */
    public function selectByID($id)
    {
        $query ="Select * From `locations` where `id` = '{$id}'; ";

        if ($result = Connect::executeQuery($query))
        {
            return (list($id, $longitude, $latitude) = Connect::fetch($result)) ?
                new Location($longitude, $latitude, $id) : null;
        }
        else
        {
            return null;
        }
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param Location $location
     * @return int|null|string
     */
    public function insert(Location $location)
    {
        $query = "Insert into `locations` values ( NULL, '{$location->getLongitude()}','{$location->getLatitude()}'); ";

        return (Connect::executeQuery($query)) ? Connect::insertedID() : null ;
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param Location $location
     * @return bool|\mysqli_result
     */
    public function delete(Location $location)
    {
        $query = "delete * from `locations`  where `id` = '{$location->getId()}'; ";

        return Connect::executeQuery($query);
    }


}