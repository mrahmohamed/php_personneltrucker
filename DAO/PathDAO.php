<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 6:43 PM
 */

namespace DAO;

$root = dirname(dirname(__FILE__));

require_once $root . '/DAO/DB/Connect.php';

require_once $root .'/DAO/LocationDAO.php';
require_once $root .'/DAO/PersonnelDAO.php';

require_once $root .'/Model/Path.php';
require_once $root .'/Model/Location.php';


use DAO\Connect;

use DAO\LocationDAO;
use DAO\PersonnelDAO;

use Model\Path;
use Model\Location;

use ArrayObject;

class PathDAO
{
    private $locationDAO;
    private $personnelDAO;

    public function __construct()
    {
        $this->locationDAO  = new LocationDAO();
        $this->personnelDAO = new PersonnelDAO();
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @return ArrayObject
     */
    public function select()
    {
        $list = new ArrayObject(array());

        $query = "Select * From `paths`; ";

        if ($result = Connect::executeQuery($query))
        {
            while (list($id, $currentDateTime, $locationID, $personnelID) = Connect::fetch($result))
            {
                $list->append(new Path($currentDateTime,$this->locationDAO->selectByID($locationID),$this->personnelDAO->selectByID($personnelID), $id));
            }
        }
        return $list;
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param $id
     * @return Path|null
     */
    public function selectByID($id)
    {
        $query ="Select * From `paths` where `id` = '{$id}'; ";

        if ($result = Connect::executeQuery($query))
        {
            return (list($id, $currentDateTime, $locationID, $personnelID) = Connect::fetch($result)) ?
                new Path($currentDateTime, $this->locationDAO->selectByID($locationID),$this->personnelDAO->selectByID($personnelID), $id) : null;
        }
        else
        {
            return null;
        }
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param Path $path
     * @return int|null|string
     */
    public function insert(Path $path)
    {
        $query = "Insert into `paths` values ( NULL, '{$path->getCurrentDateTime()}','{$path->getLocation()->getId()}','{$path->getUser()->getId()}'); ";

        return (Connect::executeQuery($query)) ? Connect::insertedID() : null ;
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param Path $path
     * @return bool|\mysqli_result
     */
    public function delete(Path $path)
    {
        $query = "delete * from `paths`  where `id` = '{$path->getId()}'; ";

        return Connect::executeQuery($query);
    }


}