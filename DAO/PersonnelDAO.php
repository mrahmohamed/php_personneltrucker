<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 6:59 PM
 */

namespace DAO;

$root = dirname(dirname(__FILE__));

require_once $root . '/DAO/DB/Connect.php';
require_once $root .'/Model/Personnel.php';

use DAO\Connect;
use Model\Personnel;

use ArrayObject;

class PersonnelDAO
{

    /* ----------------------- * * ----------------------- */

    /**
     * @return ArrayObject
     */
    public function select()
    {
        $list = new ArrayObject(array());

        $query = "Select * From `personnels`; ";

        if ($result = Connect::executeQuery($query))
        {
            while (list($id,$fullName, $CNE, $address, $phone, $email) = Connect::fetch($result))
            {
                $list->append(new Personnel($fullName, $CNE, $address, $phone, $email,$id));
            }
        }
        return $list;
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param $id
     * @return Personnel|null
     */
    public function selectByID($id)
    {
        $query ="Select * From `personnels` where `id` = '{$id}'; ";

        if ($result = Connect::executeQuery($query))
        {
            return (list($id, $fullName, $CNE, $address, $phone, $email) = Connect::fetch($result)) ?
                new Personnel($fullName, $CNE, $address, $phone, $email,$id) : null;
        }
        else
        {
            return null;
        }
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param Personnel $personnel
     * @return int|null|string
     */
    public function insert(Personnel $personnel)
    {
        $query = "Insert IGNORE into `personnels` values ( NULL, '{$personnel->getFullName()}','{$personnel->getCNE()}','{$personnel->getAddress()}','{$personnel->getPhone()}','{$personnel->getEmail()}'); ";

        return (Connect::executeQuery($query)) ? Connect::insertedID() : null ;
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param Personnel $personnel
     * @return int|null|string
     */
    public function update(Personnel $personnel)
    {
        $query = "Update `personnels` set"
                ." `fullName` = '{$personnel->getFullName()}',"
                ." `CNE`      = '{$personnel->getCNE()}',"
                ." `address`  = '{$personnel->getAddress()}',"
                ." `phone`    = '{$personnel->getPhone()}',"
                ." `email`    = '{$personnel->getEmail()}'"
                ."  where `id`= '{$personnel->getId()}'";

        return (Connect::executeQuery($query)) ? Connect::insertedID() : null ;
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param Personnel $personnel
     * @return bool|\mysqli_result
     */
    public function delete(Personnel $personnel)
    {
        $query = "delete * from `personnels`  where `id` = '{$personnel->getId()}'; ";

        return Connect::executeQuery($query);
    }
}