<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:08 PM
 */

namespace DAO;


$root = dirname(dirname(__FILE__));

require_once $root . '/DAO/DB/Connect.php';

require_once $root .'/Model/School.php';
require_once $root .'/DAO/LocationDAO.php';
require_once $root .'/Model/Location.php';

use DAO\Connect;

use Model\School;
use DAO\LocationDAO;
use Model\Location;

use ArrayObject;

class SchoolDAO
{

    private $locationDAO;

    function __construct()
    {
        $this->locationDAO = new LocationDAO();
    }


    /* ----------------------- * * ----------------------- */

    /**
     * @return ArrayObject
     */
    public function select()
    {
        $list = new ArrayObject(array ());

        $query = "Select * From `schools`; ";

        if ($result = Connect::executeQuery($query))
        {
            while (list($id, $schoolName, $address,$locationID) = Connect::fetch($result))
            {
                $list->append(new School($schoolName, $address,$this->locationDAO->selectByID($locationID), $id));
            }
        }
        return $list;
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param $id
     * @return School|null
     */
    public function selectByID($id)
    {
        $query ="Select * From `schools` where `id` = '{$id}'; ";

        if ($result = Connect::executeQuery($query))
        {
            return (list($id, $schoolName, $address,$locationID) = Connect::fetch($result)) ?
                new School($schoolName, $address,$this->locationDAO->selectByID($locationID), $id) : null;
        }
        else
        {
            return null;
        }
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param School $school
     * @return int|null|string
     */
    public function insert(School $school)
    {
        $query = "Insert into `schools` values ( NULL, '{$school->getSchoolName()}','{$school->getAddress()}','{$school->getLocation()->getId()}'); ";

        return (Connect::executeQuery($query)) ? Connect::insertedID() : null ;
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param School $school
     * @return int|null|string
     */
    public function update(School $school)
    {
        $query = "Update `personnels` set "
                ." `schoolName` = '{$school->getSchoolName()}',"
                ." `address`    = '{$school->getAddress()}', "
                ." where`id`    = '{$school->getId()}' ";

        return (Connect::executeQuery($query)) ? Connect::insertedID() : null ;
    }

    /* ----------------------- * * ----------------------- */

    /**
     * @param School $school
     * @return bool|\mysqli_result
     */
    public function delete(School $school)
    {
        $query = "delete * from `schools`  where `id` = '{$school->getId()}'; ";

        return Connect::executeQuery($query);
    }


}