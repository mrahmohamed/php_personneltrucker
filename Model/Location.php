<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 6:20 PM
 */

namespace Model;

class Location
{

    private $id;
    private $longitude;
    private $latitude;

    /**
     * @param $longitude
     * @param $latitude
     * @param null $id
     */
    function __construct($longitude, $latitude,$id = null)
    {
        $this->id = $id;
        $this->longitude = $longitude;
        $this->latitude = $latitude;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    //@override
    public function toString()
    {
        return "[N = {$this->getLongitude()} ; W = {$this->getLatitude()} ]";
    }

}