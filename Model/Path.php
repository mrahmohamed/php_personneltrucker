<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 6:35 PM
 */

namespace Model;

$root = dirname(dirname(__FILE__));

require_once $root .'/Model/Personnel.php';
require_once $root .'/Model/Location.php';

use Model\Personnel, Model\Location;

use DateTime;

class Path
{
    private $id;
    private $currentDateTime;
    private $location;
    private $personnel;

    /**
     * @param $currentDateTime
     * @param Location $location
     * @param Personnel $personnel
     * @param null $id
     */
    function __construct(DateTime $currentDateTime,Location $location,Personnel $personnel,$id=null)
    {
        $this->id = $id;
        $this->currentDateTime = $currentDateTime;
        $this->location = $location;
        $this->personnel = $personnel;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCurrentDateTime()
    {
        return $this->currentDateTime;
    }

    /**
     * @param mixed $currentDateTime
     */
    public function setCurrentDateTime(DateTime $currentDateTime)
    {
        $this->currentDateTime = $currentDateTime;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Location $location
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;
    }

    /**
     * @return Personnel
     */
    public function getPersonnel()
    {
        return $this->personnel;
    }

    /**
     * @param Personnel $personnel
     */
    public function setPersonnel($personnel)
    {
        $this->personnel = $personnel;
    }


}