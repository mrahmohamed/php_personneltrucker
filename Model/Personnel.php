<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 6:20 PM
 */

namespace Model;

class Personnel
{
    private $id;
    private $fullName;
    private $CNE;
    private $address;
    private $phone;
    private $email;

    /**
     * @param $fullName
     * @param $CNE
     * @param $address
     * @param $phone
     * @param $email
     * @param null $id
     */
    function __construct($fullName, $CNE, $address, $phone, $email,$id = null)
    {
        $this->id = $id;
        $this->fullName = $fullName;
        $this->CNE = $CNE;
        $this->address = $address;
        $this->phone = $phone;
        $this->email = $email;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getCNE()
    {
        return $this->CNE;
    }

    /**
     * @param mixed $CNE
     */
    public function setCNE($CNE)
    {
        $this->CNE = $CNE;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function toString()
    {
        return "{$this->getFullName()} - {$this->getCNE()}";
    }



}