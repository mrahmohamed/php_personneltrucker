<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:53 PM
 */

include '../Public/layout/header.php';


require_once '../../Business/PathBusiness.php';

use Business\PathBusiness;


$pathBusiness = new PathBusiness();

$pathBusiness->display();


include '../Public/layout/footer.php';