<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:53 PM
 */

include '../Public/layout/header.php';


require_once '../../Business/PersonnelBusiness.php';

use Business\PersonnelBusiness;


$personnelBusiness = new PersonnelBusiness();

$personnelBusiness->display();


include '../Public/layout/footer.php';