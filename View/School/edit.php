<?php
/**
 * Created by PhpStorm.
 * User: Kakashi
 * Date: 5/9/2015
 * Time: 7:53 PM
 */

if(isset($_POST["update"]))
{
    require_once "../../Controller/SchoolController.php";
    use Controller\SchoolController;

    new SchoolController($_POST);

    // redirect to index
    header("Location: index.php");
}

include '../Public/layout/header.php';

print "<form name='update' method='POST'>";

print "<input type='hidden' name='type' value='1'>";
print '</form>';

include '../Public/layout/footer.php';